#import <XCTest/XCTest.h>
#import "lab_2_18.hpp"

@interface Tests : XCTestCase

@end

@implementation Tests

- (void)testDefaultConstructor{
    SecondLab::Astroid astroid;
    XCTAssertNoThrow(astroid);
    XCTAssertEqual(6*M_PI, astroid.Square());
    XCTAssertEqual(24, astroid.WholeLength());
}

- (void)testInitialConstructor{
    SecondLab::Astroid astroid_0(2);
    XCTAssertNoThrow(astroid_0);
    XCTAssertEqual(24*M_PI, astroid_0.Square());
    XCTAssertEqual(48, astroid_0.WholeLength());
}

- (void)testSetter{
    SecondLab::Astroid astroid;
    XCTAssertNoThrow(astroid.set(4.23));
    XCTAssertEqual(24*4.23, astroid.WholeLength());
    XCTAssertEqual(3./8*M_PI*4.23*4.23*16, astroid.Square());
    XCTAssertThrows(astroid.set(-10));
}

- (void)testGetArcLength{
    SecondLab::Astroid astroid(0.34);
    int k = int(2*2/M_PI);
    double res = k*1.5*0.34*4+1.5*0.34*4*sin(2)*sin(2);
    XCTAssertEqual(res, astroid.ArcLength(2));
}

- (void)testGetCurvatureRadius{
    SecondLab::Astroid astroid(5.3);
    double r = 1.5*5.3*4*sin(2*1);
    XCTAssertEqual(r, astroid.CurvatureRadius(1));
}

- (void)testGetDecartesCoordinates{
    SecondLab::Coordinates point;
    SecondLab::Astroid astroid(2.28);
    point.x = 2.28*4*cos(1)*cos(1)*cos(1);
    point.y = 2.28*4*sin(1)*sin(1)*sin(1);
    XCTAssertEqual(point.x, astroid.Decartes(1.).x);
    XCTAssertEqual(point.y, astroid.Decartes(1.).y);
}

- (void)testGetYCoordinate{
    SecondLab::Astroid astroid(6.84);
    XCTAssertEqual(pow((pow(6.84*4, 2./3)-pow(3.98, 2./3)), 1.5), astroid.y(3.98));
    XCTAssertNoThrow(astroid.y(-9));
    XCTAssertThrows(astroid.y(-100));
    XCTAssertNoThrow(astroid.y(0.99));
}
@end
