#include "gtest/gtest.h"
#include "lab_2_18.cpp"
#include <cmath>

using namespace SecondLab;

TEST(Astroid_Constructor, Default_Constructor){
    Astroid astroid;
    ASSERT_NO_THROW(astroid);
    ASSERT_EQ(18.8496, astroid.Square());
    ASSERT_EQ(24, astroid.WholeLength())
}

TEST(Astroid_Constructor, Initial_Constructor){
    Astroid astroid_0(2);
    ASSERT_NO_THROW(astroid_0);
    ASSERT_EQ(75.3982, astroid_0.Square());
    ASSERT_EQ(48, astroid_0.WholeLength());
    ASSERT_ANY_THROW(Astroid astroid_1(-1.09));
    ASSERT_ANY_THROW(Astroid astroid_2(0));
}

TEST(Astroid_Methods, Setters){
    Astroid astroid;
    ASSERT_NO_THROW(astroid.set(4.23));
    ASSERT_EQ(101.52, astroid.WholeLength());
    ASSERT_EQ(337.273, astroid.Square());
    ASSERT_ANY_THROW(astroid.set(-10));
}

TEST(Astroid_Methods, Arc_Length){
    Astroid astroid(0.34);
    int k = int(2*2/M_PI);
    double res = k*1.5*0.34+1.5*0.34*sin(2)*sin(2);
    ASSERT_EQ(res, astroid.ArcLength(2));
}

TEST(Astroid_Methods, Curvature_Radius){
    Astroid astroid(5.3);
    double r = 1.5*5.3*sin(2*1);
    ASSERT_EQ(r, astroid.CurvatureRadius(1));
}

TEST(Astroid_Methods, Decartes_Coordinates){
    Coordinates point;
    Astroid astroid(2.28);
    point.x = 2.28*cos(1)*cos(1)*cos(1);
    point.y = 2.28*sin(1)*sin(1)*sin(1);
    ASSERT_EQ(point, astroid.Decartes(1.));
}

TEST(Astroid_Methods, Y_Coordinate){
    Astroid astroid(6.84);
    ASSERT_EQ(16.834, astroid.y(3.98));
    ASSERT_ANY_THROW(astroid.y(-9));
    ASSERT_ANY_THROW(astroid.y(-100));
    ASSERT_ANY_THROW(astroid.y(0.99));
}

int main(int argc, char *argv[]){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
