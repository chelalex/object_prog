#ifndef dialog_hpp
#define dialog_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/Chests/Player/Player.hpp"
#include <ctime>

namespace Game{

    void GameInit(Player& player, int playernumber);//начало игры + указать сколько игроков
    
    void RangMenu();
    void NumberMenu();
    void SuitMenu();
    void OwnCardsMenu(Player& player);
    void ChestMenu (Player& player);

    std::string NewCardsMessage(struct CardItem* newcards, int number);
    void ExistenceOfCards (std::string list[], std::array<Player, 6> player);
    void LiveDialogFromMe(Player& Me, Player& Next, const DeckCard deck, int& order);
    void DefaultDialog (Player& player1, Player& player2, const DeckCard deck, int& order);//диалог между игроками-компьютерами, вводится только результат диалога
    
    void GameEnd (std::array<Player, 6> player, int playernumber);//завершение игры + вывод сундуков + объявление победителя
    template <typename T>
    T getNum(const char *msg) {
        std::cout << msg;
        T num = 0;
        while (!(std::cin >> num)) {
            std::cout << "Try again: ";
            if (std::cin.eof()) {
                break;
            }
            std::cin.clear();
            std::cin.ignore();
        }
        return num;
    }
}

#endif /* dialog_hpp */
