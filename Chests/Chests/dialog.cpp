#include "dialog.hpp"

namespace Game{

    void GameInit(Player& player, int playernumber){
        char c;
        do{
        std::cout << "Welcome to the \"Chests\" game! " << std::endl<<
        "There will be you and " << playernumber - 1 << " computer players." << std::endl <<
        "Everyone of you will get 4 cards. Your task is to get much more chests: groups of 4 cards with the same rang. You'll become a winner, if you have the biggest number of chests." << std::endl << std::endl <<
            "Did you understand the rules? (y/n)";
            std::string msg = "";
            do{
                std::cout << msg << std::endl;
                fflush(stdin);
                c = getchar();
                msg = "Try again: ";
            }while (c != 'y' && c != 'n');
        }while(c != 'y');
        c = '\0';
        do{
            std::cout << "Your name is: " << player.GetName(player) << std::endl;
            std::cout << "Do you like your name? (y/n)";
            std::string msg = "";
            do{
                std::cout << msg << std::endl;
                fflush(stdin);
                c = getchar();
                msg = "Try again: ";
            }while (c != 'y' && c != 'n');
            if (c == 'n'){
                std::string Name;
                std::cout << "Input your new name: ";
                std::cin >> Name;
                player.SetName(Name);
            }
        }while (c != 'y');
        std::cout << "Good luck!" << std::endl;
    }

    void RangMenu(){
        std::cout << "Do you have... (你有。。。吗？）" << std::endl <<
        "1. ...2? （张2牌）" << std::endl <<
        "2. ...3?（张3牌）" << std::endl <<
        "3. ...4?（张4牌）" << std::endl <<
        "4. ...5?（张5牌）" << std::endl <<
        "5. ...6?（张6牌）" << std::endl <<
        "6. ...7?（张7牌）" << std::endl <<
        "7. ...8?（张8牌）" << std::endl <<
        "8. ...9?（张9牌）" << std::endl <<
        "9. ...10?（张10牌）" << std::endl <<
        "10. ...jacks?（张杰克牌）" << std::endl <<
        "11. ...queens?（张皇后牌）" << std::endl <<
        "12. ...kings?（张王牌）" << std::endl <<
        "13. ...aces?（张爱司牌）" << std::endl << std::endl;
    }
    
    void NumberMenu(){
        std::cout << "You have ..." << std::endl <<
        "1. ...1 card, don't you?" << std::endl <<
        "2. ...2 cards, don't you?" << std::endl <<
        "3. ...3 cards, don't you?" << std::endl << std::endl;
    }

    void SuitMenu(){
        std::cout << "You have ... " << std::endl <<
        "1. ...spade one, right?" << std::endl <<
        "2. ...club one, right?" << std::endl <<
        "3. ...diamond one, right?" << std::endl <<
        "4. ...heart one, right?" << std::endl << std::endl;
    }

    void OwnCardsMenu(Player& player){
        std::cout << "Cards in your hands are:" << std::endl << player.OutputCards(player) << std::endl << std::endl;
    }

    void ChestMenu (Player& player){
        std::cout << "You have chests of " << player.GetChests(player) << std::endl << std::endl;
    }

    std::string NewCardsMessage(struct CardItem* newcards, int number){
        std::string list;
        if (newcards){
            CardItem *current = newcards;
            for (int i = 0; i < number; i++){
                int rang = current->card.rang;
                int suit = current->card.suit;
                switch (suit){
                    case 0: list += "spade "; break;
                    case 1: list += "club "; break;
                    case 2: list += "diamond "; break;
                    case 3: list += "heart "; break;
                    default: break;
                }
                switch(rang){
                    case 11: list += "Jack"; break;
                    case 12: list += "Queen"; break;
                    case 13: list += "King"; break;
                    case 14: list += "Ace"; break;
                    default: list += std::to_string(rang); break;
                }
                if (i+1 < number) list += ", ";
                current = current->next;
            }
        }
        else list = "nothing";
        return list;
    }

    void LiveDialogFromMe(Player& Me, Player& Next, const DeckCard deck, int& order){
        int rang, number = -1, suit;
        struct CardItem *newcards = nullptr;
        RangMenu();
        OwnCardsMenu(Me);
        std::string msg = "";
        bool flag;
        do{
            std::cout << msg;
            fflush(stdin);
            rang = getNum<int>("Your choice: ");
            rang += 1;
            flag = Me.CheckRang(Me, rang);
            if (rang < 2 || rang > 14) msg = "Rang is out. ";
            if (!flag) msg = "You may not ask this rang. ";
        }while((rang < 2 || rang > 14) || (!flag));
        msg = "";
        flag *= Next.CheckRang(Next, rang);
        if (!flag){
            std::cout << "Sorry, you didn't guess" << std::endl;
            std::cout << Me.GetName(Me) << " got from " << Next.GetName(Next) << " " << NewCardsMessage(newcards, number) << std::endl << std::endl;
            if (order != 52) Me.SetCardFromDeck(deck, order);
            OwnCardsMenu(Me);
            return;
        }
        else{
            std::cout << "You're right, let's continue." << std::endl << std::endl;
            NumberMenu();
            OwnCardsMenu(Me);
            msg = "";
            do{
                fflush(stdin);
                number = getNum<int>("Your choice: ");
                std::cout << msg;
                msg = "Try again: ";
            }while(number < 1 && number > 3);
            flag *= Next.CheckRangNumber(Next, number, rang);
            if (!flag){
                std::cout << "Sorry, you didn't guess" << std::endl;
                std::cout << Me.GetName(Me) << " got from " << Next.GetName(Next) << " " << NewCardsMessage(newcards, number) << std::endl << std::endl;
                if (order != 52) Me.SetCardFromDeck(deck, order);
                OwnCardsMenu(Me);
                return;
            }
            else{
                std::cout << "You're right, let's continue." << std::endl << std::endl;
                SuitMenu();
                OwnCardsMenu(Me);
                msg = "";
                for (int i = 1; i <= number; i++){
                    do{
                        fflush(stdin);
                        suit = getNum<int>("Your choice: ");
                        std::cout << msg;
                        msg = "Try again: ";
                    }while(suit < 1 && suit > 4);
                    suit -= 1;
                    flag *= Next.CheckRangSuits(Next, rang, suit);
                    if (!flag){
                        std::cout << "Sorry, you didn't guess" << std::endl;
                        std::cout << Me.GetName(Me) << " got from " << Next.GetName(Next) << " " << NewCardsMessage(newcards, number) << std::endl << std::endl;
                        if (order != 52) Me.SetCardFromDeck(deck, order);
                        OwnCardsMenu(Me);
                        return;
                    }
                    else msg = "";
                }
                newcards = Next.GetCards(Next, rang, deck, order);
                Me.SetCards(newcards, deck, order);
                std::cout << Me.GetName(Me) << " got from " << Next.GetName(Next) << " " << NewCardsMessage(newcards, number) << std::endl << std::endl;
            }
        }
    }

    void DefaultDialog (Player& player1, Player& player2, const DeckCard deck, int& order){
        srand(time(0));
        int rang, number = -1, suit;
        bool flag;
        struct CardItem *newcards = nullptr;
        do{
            rang = 2 + rand()%13;
            flag = player1.CheckRang(player1, rang);
        }while(!flag);
        flag *= player2.CheckRang(player2, rang);
        if (!flag){
            std::cout << player1.GetName(player1) << " got from " << player2.GetName(player2) << " " << NewCardsMessage(newcards, number) << std::endl << std::endl;
            if (order != 52) player1.SetCardFromDeck(deck, order);
            return;
        }
        else{
            do{
                number = 1 + rand()%3;
            }while(player1.CheckRangNumber(player1, number, rang) == false);
            number = 4 - number;
            number = 1 + rand()%number;
            flag *= player2.CheckRangNumber(player2, number, rang);
            if (!flag){
                std::cout << player1.GetName(player1) << " got from " << player2.GetName(player2) << " " << NewCardsMessage(newcards, number) << std::endl << std::endl;
                if (order != 52) player1.SetCardFromDeck(deck, order);
                return;
            }
            else{
                for (int i = 1; i <= number; i++){
                    do{
                        suit = rand()%4;
                    }while(player1.CheckRangSuits(player1, rang, suit) == true);
                    
                    flag *= player2.CheckRangSuits(player2, rang, suit);
                    if (!flag){
                        std::cout << player1.GetName(player1) << " got from " << player2.GetName(player2) << " " << NewCardsMessage(newcards, number) << std::endl << std::endl;
                        if (order != 52) player1.SetCardFromDeck(deck, order);
                        return;
                    }
                }
                newcards = player2.GetCards(player2, rang, deck, order);
                player1.SetCards(newcards, deck, order);
                std::cout << player1.GetName(player1) << " got from " << player2.GetName(player2) << " " << NewCardsMessage(newcards, number) << std::endl << std::endl;
            }
        }
    }

    void GameEnd(std::array<Player, 6> player, int playernumber){
        int chestnumbers[playernumber];
        for (int i = 0; i < playernumber; i++){
            chestnumbers[i] = 0;
            std::string line = player[i].GetChests(player[i]);
            std::cout << player[i].GetName(player[i]) << " has chests of " << line << std::endl << std::endl;
            while (line.size()){
                int pos = (int)line.find(" ");
                if (pos > 0){
                    chestnumbers[i]++;
                    line.erase(0, pos+1);
                }
                else line.clear();
            }
        }
        int max_number = 0;
        for (int i = 0; i < playernumber; i++){
            if (chestnumbers[max_number] < chestnumbers[i])
                max_number = i;
        }
        std::cout << player[max_number].GetName(player[max_number]) << " is WINNER!!!" << std::endl << std::endl;
    }

    void ExistenceOfCards (std::string list[], std::array<Player, 6> player){
        for (int i = 0; i < 6; i++)
            list[i] = player[i].OutputCards(player[i]);
    }
}
