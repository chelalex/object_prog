#include "dialog.hpp"

using namespace Game;


int order = 0;
const DeckCard deck(52);

int main(){
    int playernumber;
    std::cout << "Choose number of players (2 - 6), please." << std::endl;
    fflush(stdin);
    playernumber = getNum<int>("Your choice: ");
    std::array<Player, 6> player = {Player(0, deck, order, playernumber), Player(1, deck, order, playernumber), Player(2, deck, order, playernumber), Player(3, deck, order, playernumber), Player(4, deck, order, playernumber), Player(5, deck, order, playernumber)};
    std::string list[6];
    GameInit(player[0], playernumber);
    int current = 0, next = 1;
    do{
        if (current == 0) LiveDialogFromMe(player[current], player[next], deck, order);
        else DefaultDialog(player[current], player[next], deck, order);
        int count = 0;
        do{
            current = (current + 1) % playernumber;
            count++;
        }while(player[current].OutputCards(player[current]) == "nothing" && count != playernumber);//count - счетчик, проверяющий, прошлись ли мы по кругу игроков
        count = 0;
        do{
            next++;
            count++;
        }while((player[next % playernumber].OutputCards(player[next % playernumber]) == "nothing" || next <= current) && count != playernumber);
        next %= playernumber;
        
        ExistenceOfCards(list, player);
        
    }while(order != 52 || !(list[0] == list[1] && list[1] == list[2] && list[2] == list[3] && list[3] == list[4] && list[4] == list[5] && list[5] == "nothing"));
    GameEnd(player, playernumber);
    return 0;
}

