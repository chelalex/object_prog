#ifndef Player_hpp
#define Player_hpp

#include <iostream>
#include <cstring>
#include "/Users/aleksandracelnokova/Desktop/kaf12/CardLib/Card.h"
#include <array>
using namespace Prog3;

namespace Game{

    struct Chest{
        struct Card cards[4];//готовый сундук из 4 карт одного ранга
    };
    
    struct ChestItem{
        struct Chest chest;
        struct ChestItem *next;
    };

    struct CardItem{
        struct Card card;
        struct CardItem *next;
        struct CardItem& operator= (struct CardItem& card){
            this->card.rang = card.card.rang;
            this->card.suit = card.card.suit;
            this->next = card.next;
            return *this;
        }
    };
    

    //const DeckCard deck(52);

    class Player{
        private:
            std::string name;//имя игрока, чтобы выводить, кто кого спросил из игроков
            struct ChestItem *chest;//указатель на список сундуков
            struct CardItem *cards;//список карт, которые на руках у игрока
            int cardnumber;//количество карт на руках
            
        public:
            Player(const int i, const DeckCard& deck, int& order, const int playernumber);//конструктор, создающий игрока-компьютер, order - первая карта в колоде
            //Player(std::array<Player, 6> player, const DeckCard& deck, int& order);
            Player& SetName(std::string Name);//сеттер, присваивает имя игроку
            std::string GetName(Player& player);
            std::string GetChests(Player& player);//вывод рангов сундуков
            std::string OutputCards(Player& player);//вывод собственных карт
            friend void Swap(CardItem**, CardItem** );//смена значений местами
            Player& SortCards();//сортировка карт по рангу
            bool CheckRang (Player& player, int rang);//угадать наличие определенного ранга
            bool CheckRangNumber (Player& player, int number, int rang);//угадать количество карт определенного ранга
            bool CheckRangSuits (Player& player, int rang, int suit);//угадать все масти всех карт угаданного ранга
            struct CardItem* GetCards(Player& player, int rang, const DeckCard& deck, int& order);//выдача карт другому игроку
            Player& SetCards(struct CardItem *cards, const DeckCard& deck, int& order);//получение карт от другого игрока
            Player& SetCardFromDeck (const DeckCard& deck, int& order);//взятие карты из колоды
            Player& MakeChest();//сбор карт в сундук
            
    };
}

#endif /* Player_hpp */
