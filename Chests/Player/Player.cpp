#include "Player.hpp"

namespace Game{
    
    Player::Player(const int i, const DeckCard& deck, int& order, const int playernumber){
        std::string part1 = "Player";
        std::string part2 = std::to_string(i);
        this->name = part1+part2;
        this->chest = nullptr;
        this->cardnumber = 0;
        this->cards = nullptr;
        const int n = order + 3;
        CardItem head = {{-1, -1}, this->cards};
        CardItem *previous = &head;
        CardItem *currentitem = head.next;
        if (i <= playernumber - 1){
            for (;order <= n; order++){
                currentitem = new CardItem;
                previous->next = currentitem;
                struct Card current;
                current.rang = deck.GetRang(order);
                current.suit = deck.GetSuit(order);
                currentitem->card = current;
                currentitem->next = nullptr;
                previous = currentitem;
                currentitem = previous->next;
                this->cardnumber++;
            }
            this->cards = head.next;
        }
        else{
            this->cards = nullptr;
        }
    }

    Player& Player::SetName(std::string Name){
        this->name = Name;
        return *this;
    }

    std::string Player::GetName(Player& player){
        return player.name;
    }
    
    std::string Player::GetChests(Player& player){
        ChestItem *current = player.chest;
        std::string list = "";
        if (!current) return "nothing";
        while (current){
            int n = current->chest.cards[2].rang;
            switch(n){
                case 11: list += "Jacks"; break;
                case 12: list += "Queens"; break;
                case 13: list += "Kings"; break;
                case 14: list += "Aces"; break;
                default: list += std::to_string(n); break;
            }
            if (current->next) list += ", ";
            current = current->next;
        }
        return list;
    }

    std::string Player::OutputCards(Player& player){
        CardItem *current = player.cards;
        std::string list = "";
        if (!current) return "nothing";
        while (current){
            int rang = current->card.rang;
            int suit = current->card.suit;
            switch (suit){
                case 0: list += "spade "; break;
                case 1: list += "club "; break;
                case 2: list += "diamond "; break;
                case 3: list += "heart "; break;
                default: break;
            }
            switch(rang){
                case 11: list += "Jack"; break;
                case 12: list += "Queen"; break;
                case 13: list += "King"; break;
                case 14: list += "Ace"; break;
                default: list += std::to_string(rang); break;
            }
            if (current->next) list += ", ";
            current = current->next;
        }
        return list;
    }

    void Swap(CardItem** card1, CardItem** card2){
        CardItem *temp = nullptr;
        temp = *card2;
        *card2 = *card1;
        *card1 = temp;
    }

    Player& Player::SortCards(){
        CardItem **list = &this->cards;
        CardItem head = {{-1, -1}, *list};
        CardItem *previous = &head;
        if (this->cardnumber > 1){
            CardItem *card1 = previous->next;
            CardItem *card2 = card1->next;
            int number = this->cardnumber;
            for (int i = 0; i < number; i++){
                while (card1){
                    if(card2){
                        if (card1->card.rang > card2->card.rang){
                            card1->next = card2->next;
                            card2->next = card1;
                            previous->next = card2;
                            Swap(&card1, &card2);
                        }
                        previous = card1;
                        card1 = previous->next;
                        card2 = card1->next;
                    }
                    else break;
                }
                previous = &head;
                card1 = previous->next;
                card2 = card1->next;
            }
        }
        this->cards = head.next;
        return *this;
    }

    bool Player::CheckRang (Player& player, int rang){
        CardItem *current = player.cards;
        while (current){
            if (current->card.rang == rang)
                return true;
            current = current->next;
        }
        return false;
    }

    bool Player::CheckRangNumber (Player& player, int number, int rang){
        CardItem *current = player.cards;
        while (current->card.rang != rang) current = current->next;
        int i = 0;
        while (current && current->card.rang == rang){
            i++;
            current = current->next;
        }
        return i==number?true:false;
    }

    bool Player::CheckRangSuits (Player& player, int rang, int suit){
        CardItem *current = player.cards;
        while (current->card.rang != rang) current = current->next;
        while (current){
            if (current->card.rang == rang){
                if (current->card.suit == suit)
                    return true;
            }
            current = current->next;
        }
        return false;
    }

    struct CardItem* Player::GetCards(Player& player, int rang, const DeckCard& deck, int& order){
        CardItem head = {{-1, -1}, player.cards};
        CardItem *begin = head.next;
        CardItem *previous = &head;
        while (begin->card.rang != rang){
            begin = begin->next;
            previous = previous->next;
        }
        CardItem *end = begin;
        int n = 1;
        while (end->next){
            if (end->next->card.rang == rang){
                end = end->next;
                n++;
            }
            else break;
        }
        if (end->next)
            previous->next = end->next;
        else
            previous->next = nullptr;
        end->next = nullptr;
        player.cards = head.next;
        player.cardnumber -= n;
        while(player.cardnumber < 4 && order != 52){
            player.SetCardFromDeck(deck, order);
        }
        return begin;
    }

    Player& Player::SetCards(struct CardItem *cards, const DeckCard& deck, int& order){
        CardItem head = {{-1, -1}, this->cards};
        CardItem *current = head.next;
        CardItem *begin = cards;
        CardItem *end = begin;
        int n = 1;
        while (end->next) {end = end->next; n++;}
        end->next = current;
        head.next = begin;
        this->cards = head.next;
        this->cardnumber += n;
        this->SortCards();
        this->MakeChest();
        while(this->cardnumber < 4 && order != 52){
            this->SetCardFromDeck(deck, order);
        }
        return *this;
    }

    Player& Player::SetCardFromDeck (const DeckCard& deck, int& order){
        CardItem head = {{-1, -1}, this->cards};
        CardItem *previous = &head;
        CardItem *current = previous->next;
        while (current){
            previous = previous->next;
            current = previous->next;
        }
        int rang = deck.GetRang(order);
        int suit = deck.GetSuit(order);
        order++;
        current = new CardItem;
        previous->next = current;
        current->card.rang = rang;
        current->card.suit = suit;
        current->next = nullptr;
        this->cardnumber++;
        this->SortCards();
        this->MakeChest();
        return *this;
    }

    Player& Player::MakeChest(){
        Card emptycard = {-1, -1};
        CardItem headcard = {emptycard, this->cards};
        CardItem *begin = &headcard;
        CardItem *end = begin->next;
        Chest emptychest = {emptycard, emptycard, emptycard, emptycard};
        ChestItem headchest = {emptychest, this->chest};
        ChestItem *current = headchest.next;
        int n = 1;
        while (begin && end){
            while (end->next && end->card.rang == end->next->card.rang){
                end = end->next;
                n++;
            }
            if (n == 4){
                ChestItem *temp = new ChestItem;
                CardItem *tail = begin;
                begin = begin->next;
                for (int i = 0; i < n; i++){
                    temp->chest.cards[i].rang = begin->card.rang;
                    temp->chest.cards[i].suit = begin->card.suit;
                    begin = begin->next;
                }
                this->cardnumber -= 4;
                temp->next = current;
                headchest.next = temp;
                tail->next = end->next;
            }
            begin = end;
            end = end->next;
            current = headchest.next;
            n = 1;
        }
        this->chest = headchest.next;
        this->cards = headcard.next;
        return *this;
    }
}
