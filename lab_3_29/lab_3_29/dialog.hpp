#ifndef dialog_hpp
#define dialog_hpp

#include <iostream>
#include "/Users/aleksandracelnokova/Desktop/kaf12/Lab_3_29/CharSet/lab_3_29.hpp"

namespace ThirdLab{
    void dialog (CharSet& charset, bool flag = 0);
    void print_menu();
    CharSet& ConstructWithInt();
    CharSet& ConstructWithChar();
    void GetNewCharSet (CharSet& charset);
    void OutputCharSet (CharSet& charset);
    void CheckElement (CharSet& charset);

    void SubtractSets (CharSet& charset);
    void InjectNewElement (CharSet& charset);
    void MultiplicateSets (CharSet& charset);
    void AddSets (CharSet& charset);
    //функции, связанные с операторами
    void Operator_SubtractSets (CharSet& charset);
    void Operator_InjectNewElement (CharSet& charset);
    void Operator_MultiplicateSets (CharSet& charset);
    void Operator_AddSets (CharSet& charset);
    
    template <typename T>
    T getNum(const char *msg) {
        std::cout << msg;
        T num = 0;
        while (!(std::cin >> num)) {
            std::cout << "Try again: ";
            if (std::cin.eof()) {
                break;
            }
            std::cin.clear();
            std::cin.ignore();
        }
        return num;
    }
}
#endif /* dialog_hpp */
