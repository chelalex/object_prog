//
//  main.cpp
//  lab_3_29
//
//  Created by Александра Челнокова on 09.10.2020.
//

#include "dialog.hpp"

using namespace ThirdLab;

int main() {
    try{
        CharSet charset;
        std::cout << "Do you wanna check operators or methods (o/m)? ";
        char *msg = "";
        char c;
        do{
            puts(msg);
            c = getchar();
            msg = "You're wrong, try again";
        }while(c != 'o' && c != 'm');
        if (c == 'o') dialog(charset, 1);
        else dialog(charset, 0);
    }
    catch (const std::invalid_argument &error){
        std::cerr << error.what() << std::endl;
    }
    return 0;
}
