#include "dialog.hpp"
#define options_number 6

namespace ThirdLab{
    
    typedef void(*opt_t)(CharSet&);

    static const opt_t options[options_number] = {&OutputCharSet, &CheckElement, &SubtractSets, &InjectNewElement, &MultiplicateSets, &AddSets};
    
    static const opt_t operator_options[options_number] = {&OutputCharSet, &CheckElement, &Operator_SubtractSets, &Operator_InjectNewElement, &Operator_MultiplicateSets, &Operator_AddSets};


    void dialog (CharSet& charset, bool flag){
        int choice = 0;
        std::cout << "------------------------------------------------------------" << std::endl;
        while (true) {
            print_menu();
            std::cout << "------------------------------------------------------------" << std::endl;
            choice = getNum<int>("Your choice: ");
            std::cout << "------------------------------------------------------------" << std::endl;
            if (!choice)
                      break;
            else if (choice < 0 || choice > options_number){
                    std::cout << "Invalid option detected!" << std::endl;
                   }
                else{
                    if (!flag) options[choice - 1](charset);
                    else operator_options[choice - 1](charset);
                }
            std::cout << "------------------------------------------------------------" << std::endl;
                }
        }

    void print_menu(){
        std::cout << "0. Quit" << std::endl <<
        "1. Output current char set" << std::endl <<
        "2. Check existence of an element" << std::endl <<
        "3. Subtract from your set any set you'll input" << std::endl <<
        "4. Inject any element" << std::endl <<
        "5. Multiplicate your set with any set you'll input" << std::endl <<
        "6. Add any set you'll input to your set" << std::endl;
    
    }

    CharSet& ConstructWithInt(){
        int n = getNum<int>("Your choice: ");
        CharSet charset(n);
        return charset;
    }

    CharSet& ConstructWithChar(){
        char* line = nullptr;
        char* msg = "";
        do{
            puts(msg);
            std::cout << "Input the line of chars: ";
            std::cin >> line;
            msg = "Too big! Try again!";
        }while(strlen(line) > 100);
        CharSet charset(line);
        return charset;
    }

    void GetNewCharSet (CharSet& charset){
        char* msg = "";
        do{
            puts(msg);
            std::cout << "Input the line of chars: ";
            std::cin >> charset;
            msg = "Too big! Try again!";
        }while(!std::cin.good());
    }

    void OutputCharSet (CharSet& charset){
        std::cout << charset;
    }

    void CheckElement (CharSet& charset){
        char symbol;
        char *msg = "";
            puts(msg);
            std::cout << "Input a symbol: ";
            fflush(stdin);
            symbol = getchar();
            msg = "Too big! Try again!";
        bool flag = charset.Existence(&symbol, charset);
        std::cout << flag << std::endl;
    }

    void SubtractSets (CharSet& charset){
        CharSet new_charset;
        GetNewCharSet(new_charset);
        charset.Subtraction(new_charset);
    }

    void InjectNewElement (CharSet& charset){
        char symbol;
        char *msg = "";
            puts(msg);
            std::cout << "Input a symbol: ";
            fflush(stdin);
            symbol = getchar();
            msg = "Too big! Try again!";
        if (!charset.Existence(&symbol, charset)) charset.Injection(&symbol);
    }

    void MultiplicateSets (CharSet& charset){
        CharSet new_charset;
        GetNewCharSet(new_charset);
        charset.Multiplication(new_charset);
    }

    void AddSets (CharSet& charset){
        CharSet new_charset;
        GetNewCharSet(new_charset);
        charset.Addition(new_charset);
    }

    void Operator_SubtractSets(CharSet& charset){
        CharSet new_charset;
        GetNewCharSet(new_charset);
        charset - new_charset;
    }

    void Operator_InjectNewElement (CharSet& charset){
        char symbol;
        char *msg = "";
            puts(msg);
            std::cout << "Input a symbol: ";
            fflush(stdin);
            symbol = getchar();
            msg = "Too big! Try again!";
        charset += &symbol;
    }

    void Operator_MultiplicateSets (CharSet& charset){
        CharSet new_charset;
        GetNewCharSet(new_charset);
        charset * new_charset;
    }

    void Operator_AddSets (CharSet& charset){
        CharSet new_charset;
        GetNewCharSet(new_charset);
        charset + new_charset;
    }
}

