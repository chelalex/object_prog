#include "dialog_dyn.hpp"
#define options_number 7

namespace ThirdLab_dyn{
    typedef void(*opt_t)(CharSet_dyn&);

    static const opt_t options[options_number] = {&OutputCharSet, &CheckElement, &SubtractSets, &InjectNewElement, &MultiplicateSets, &AddSets, &AddLine};
    
    void dialog (CharSet_dyn& charset){
        int choice = 0;
        std::cout << "------------------------------------------------------------" << std::endl;
        while (true) {
            print_menu();
            std::cout << "------------------------------------------------------------" << std::endl;
            choice = getNum<int>("Your choice: ");
            std::cout << "------------------------------------------------------------" << std::endl;
            if (!choice)
                    break;
            else if (choice < 0 || choice > options_number){
                    std::cout << "Invalid option detected!" << std::endl;
                }
            else{
                options[choice - 1](charset);
            }
            std::cout << "------------------------------------------------------------" << std::endl;
        }
    }
    
    void print_menu(){
        std::cout << "0. Quit" << std::endl <<
        "1. Output current char set" << std::endl <<
        "2. Check existence of an element" << std::endl <<
        "3. Subtract from your set any set you'll input" << std::endl <<
        "4. Inject any element" << std::endl <<
        "5. Multiplicate your set with any set you'll input" << std::endl <<
        "6. Add any set you'll input to your set" << std::endl <<
        "7. Add a line you'll input to your set" << std::endl;
    }

void GetNewCharSet (CharSet_dyn& charset){
    char* msg = "";
    do{
        puts(msg);
        std::cout << "Input the line of chars: ";
        std::cin >> charset;
        msg = "Try again!";
    }while(!std::cin.good());
}

void OutputCharSet (CharSet_dyn& charset){
    std::cout << charset;
}

void CheckElement (CharSet_dyn& charset){
    char symbol;
    char *msg = "";
        puts(msg);
        std::cout << "Input a symbol: ";
        fflush(stdin);
        symbol = getchar();
        msg = "Too big! Try again!";
    bool flag = Existence(&symbol, charset);
    std::cout << flag << std::endl;
}

void SubtractSets (CharSet_dyn& charset){
    CharSet_dyn new_charset;
    GetNewCharSet(new_charset);
    charset - new_charset;
}

void InjectNewElement (CharSet_dyn& charset){
    char symbol;
    std::cout << "Input a symbol: ";
    fflush(stdin);
    symbol = getchar();
    charset += &symbol;
}

void MultiplicateSets (CharSet_dyn& charset){
    CharSet_dyn new_charset;
    GetNewCharSet(new_charset);
    charset * new_charset;
}

void AddSets (CharSet_dyn& charset){
    CharSet_dyn new_charset;
    GetNewCharSet(new_charset);
    charset + new_charset;
}

void AddLine (CharSet_dyn& charset){
    char* msg = "";
    CharSet_dyn newcharset;
    do{
        puts(msg);
        std::cout << "Input the line of chars: ";
        std::cin >> newcharset;
        msg = "Try again!";
    }while(!std::cin.good());
    charset + newcharset;
}

}
