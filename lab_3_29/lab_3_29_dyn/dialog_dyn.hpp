#ifndef dialog_dyn_hpp
#define dialog_dyn_hpp

#include <iostream>
#include "/Users/aleksandracelnokova/Desktop/kaf12/Lab_3_29/CharSet_dyn/lab_3_29_dyn.hpp"

namespace ThirdLab_dyn{
    void dialog (CharSet_dyn& charset);
    void print_menu();
    void GetNewCharSet (CharSet_dyn& charset);
    void OutputCharSet (CharSet_dyn& charset);
    void CheckElement (CharSet_dyn& charset);

    void SubtractSets (CharSet_dyn& charset);
    void InjectNewElement (CharSet_dyn& charset);
    void MultiplicateSets (CharSet_dyn& charset);
    void AddSets (CharSet_dyn& charset);
    void AddLine (CharSet_dyn& charset);
    template <typename T>
    T getNum(const char *msg) {
        std::cout << msg;
        T num = 0;
        while (!(std::cin >> num)) {
            std::cout << "Try again: ";
            if (std::cin.eof()) {
                break;
            }
            std::cin.clear();
            std::cin.ignore();
        }
        return num;
    }
}
#endif /* dialog_dyn_hpp */
