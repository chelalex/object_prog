//
//  main.cpp
//  lab_3_29_dyn
//
//  Created by Александра Челнокова on 15.10.2020.
//

#include "dialog_dyn.hpp"

using namespace ThirdLab_dyn;

int main() {
    try{
        CharSet_dyn charset;
        dialog(charset);
    }
    catch (const std::invalid_argument &error){
        std::cerr << error.what() << std::endl;
    }
    return 0;
}
