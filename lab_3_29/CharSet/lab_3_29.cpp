#include "lab_3_29.hpp"
#include <iostream>

namespace ThirdLab{

    CharSet::CharSet(int cardinal){
        if (cardinal <= 100){
            this->cardinality = cardinal;
            for (int i = 0; i < cardinal; i++){
                this->value[i] = 32+i;
            }
            for (int i = cardinal; i < 100; i++){
                this->value[i] = '\0';
            }
        }
        else throw std::invalid_argument ("Input cardinality is bigger than possible one");
    }
    

    char* DeleteIdenticalChar(char line[]){
        int n = (int)strlen(line);
        char *ptr1 = &line[0];
        char *ptr2 = ptr1 + 1;
        while (*ptr1 != '\0'){
            while(*ptr2 != '\0'){
                if (*ptr1 == *ptr2){
                    ptr2++;
                    strncpy(ptr1+1, ptr2, n-2);
                    n--;
                }
                else ptr2++;
            }
            ptr1++; n--;
            ptr2 = ptr1+1;
        }/*
        ptr1 = &line[0];
        ptr2 = ptr1;
        while (ptr2){
            while (*ptr1 != '\200') ptr1++;//находим первую пустоту
            while (*ptr2 == '\200') ptr2++;//находим первый непустой символ после пустот
            *ptr1 = *ptr2;
            ptr1++; ptr2++;
        }
        char new_line[1000];
        ptr1 = &line[0];
        ptr2 = &new_line[0];
        while (ptr1 && ptr2){
            while (*ptr1 == '\200') ptr1++;
            *ptr2 = *ptr1;
            ptr2++;
            ptr1++;
        }*/
        return line;
    }

    CharSet::CharSet(char line[]){
        int cardinal = 0;
        line = DeleteIdenticalChar(line);
        char* ptr = &line[0];
        while (*ptr != '\0'){
            if (cardinal < 100){
                this->value[cardinal] = *ptr;
                cardinal++;
                ptr++;
            }
            this->cardinality = cardinal+1;
            for (int i = cardinal; i < 100; i++){
                this->value[i] = '\0';
            }
            if (cardinal >= 100) throw std::invalid_argument ("Input char set is bigger than possible one");
        }
    }

    std::istream& operator >> (std::istream& c, CharSet& charset){
        std::string line;
        c >> line;
        charset.cardinality = 0;
        char *new_line = new char[line.size() + 1];
        std::copy(line.begin(), line.end(), new_line);
        new_line = DeleteIdenticalChar(new_line);
        if (strlen(new_line) > 100) c.setstate(std::ios::failbit);
        else{
            char *ptr = new_line;
            int i = 0;
            while (*ptr != '\0'){
                charset.value[i] = *ptr;
                charset.cardinality++;
                i++;
                ptr++;
            }
            for (; i < 100; i++)
                charset.value[i] = '\0';
        }
        return c;
    }


    std::ostream& operator << (std::ostream& s, const CharSet& charset){
        if (charset.cardinality){
            s << "Set: ";
            for (int i = 0; i < charset.cardinality; i++){
                s << charset.value[i];
            }
            s << std::endl;
            s << "Cardinality: " << charset.cardinality << std::endl;
        }
        else s << "The char set is empty";
        return s;
    }


    bool CharSet::Existence(char symbol[1], const CharSet &charset) const{
        bool flag = false;
        for (int i = 0; i < charset.cardinality; i++){
            if (charset.value[i] == *symbol){
                flag = true;
                break;
            }
        }
        return flag;
    }

    CharSet& CharSet::Equality(const CharSet &charset){
        this->cardinality = charset.cardinality;
        for (int i = 0; i < 100; i++){
            this->value[i] = charset.value[i];
        }
        return *this;
    }

    CharSet& CharSet::Subtraction (CharSet& charset){
        int cardinal = this->cardinality;
        for (int i = 0; i < cardinal; i++){
            if (Existence(&this->value[i], charset)){
                for (int j = i; j < cardinal; j++) this->value[j] = this->value[j+1];
                this->cardinality--;
                i--;
            }
        }
        return *this;
    }

    CharSet& CharSet::Injection(char symbol[1]){
        if (Existence(symbol, *this) == false){
            if (this->cardinality < 100){
                int i = 0;
                while(this->value[i] != '\0') i++;
                this->value[i] = *symbol;
                this->cardinality++;
            }
            else throw std::invalid_argument ("The char set is full");
        }
        else throw std::invalid_argument ("This symbol exists");
        return *this;
    }
    
    CharSet& CharSet::Addition(CharSet& charset){
        this->Subtraction(charset);
        if (this->cardinality + charset.cardinality <= 100){
            for (int i = 0; i < charset.cardinality; i++)
                this->Injection(&charset.value[i]);
            return *this;
        }
        else throw std::invalid_argument("Total set is too big");
    }

//перегрузка операторов

    CharSet& CharSet::operator= (const CharSet& charset){
        this->cardinality = charset.cardinality;
        for (int i = 0; i < 100; i++){
            this->value[i] = charset.value[i];
        }
        return *this;
    }

    CharSet& CharSet::operator+= (char symbol[1]){
        if (Existence(symbol, *this) == false){
            if (this->cardinality < 100){
                this->value[cardinality] = *symbol;
                this->cardinality++;
            }
            else throw std::invalid_argument ("The char set is full");
        }
        else throw std::invalid_argument ("This symbol exists");
        return *this;
    }

    CharSet& CharSet::operator- (CharSet& charset){
        int cardinal = this->cardinality;
        for (int i = 0; i < cardinal; i++){
            if (Existence(&this->value[i], charset)){
                for (int j = i; j < cardinal; j++) this->value[j] = this->value[j+1];
                this->cardinality--;
                i--;
            }
        }
        return *this;
    }

    CharSet& CharSet::operator+ (CharSet& charset){
        *this - charset;
        // = (*this) - (*this) * charset;
        if (this->cardinality + charset.cardinality <= 100){
            for (int i = 0; i < charset.cardinality; i++)
                (*this) += &charset.value[i];
            
        
        /*char buf[200];
        int cardinal = this->cardinality;
        for (int i = 0; i < cardinal; i++){
            buf[i] = this->value[i];
        }
        int cardinal1 = charset.cardinality;
        for (int i = 0; i < cardinal1; i++){
            buf[cardinal+i+1] = charset.value[i];
        }
        char *line = nullptr;
        line = DeleteIdenticalChar(buf);
        if (strlen(line) > 100) throw std::invalid_argument("Total set is too big");
        else{
            this->cardinality = 0;
            char *ptr = line;
            int i = 0;
            while (ptr){
                this->value[i] = *ptr;
                this->cardinality++;
                i++;
                ptr++;
            }
        }*/
            return *this;
        }
        else throw std::invalid_argument("Total set is too big");
}
}
