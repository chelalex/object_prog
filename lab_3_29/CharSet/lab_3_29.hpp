#ifndef lab_3_29_hpp
#define lab_3_29_hpp

#include <iostream>
#include <string.h>
namespace ThirdLab{

    class CharSet{
    private:
        char value[100];
        int cardinality;
    public:
        CharSet(int cardinal = 10);
        friend char* DeleteIdenticalChar (char line[]);
        CharSet(char line[]);
        friend std::istream& operator >> (std::istream& c, CharSet&);
        friend std::ostream& operator << (std::ostream& s, const CharSet& charset);
        CharSet& Equality(const CharSet& charset);//присваивание существующему множеству указанное
        CharSet& Subtraction (CharSet& charset);//вычитание множеств
        CharSet& Injection(char symbol[1]);//вставка нового элемента
        CharSet& Multiplication (CharSet& charset){//пересечение множеств
            CharSet new_charset;
            new_charset.Equality(*this);
            new_charset.Subtraction(charset);
            this->Subtraction(new_charset);
            return *this;
        }
        CharSet& Addition(CharSet& charset);//объединение множеств

        
        bool Existence (char symbol[1], const CharSet& charset) const;
        
        //перегрузка операторов
        CharSet& operator= (const CharSet& charset);
        CharSet& operator- (CharSet& charset);
        CharSet& operator+= (char symbol[1]);
        CharSet& operator* (CharSet& charset){
            CharSet newcharset;
            newcharset = *this;
            newcharset - charset;
            *this - newcharset;
            return *this;
        }
        CharSet& operator+ (CharSet& charset);
        
    };
}
#endif
