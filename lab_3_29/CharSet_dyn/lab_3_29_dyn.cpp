#include "lab_3_29_dyn.hpp"
#include <string.h>
#include <cstring>
namespace ThirdLab_dyn{

    CharSet_dyn::CharSet_dyn(int cardinal){
        if (cardinal > 0){
            this->value = new char[cardinal + 1];
            this->cardinality = cardinal;
            char *ptr = &this->value[0];
            int i = 0;
            while(i < cardinal){
                *ptr = 32+i;
                ptr++;
                i++;
            }
            *ptr = '\0';
        }
        else throw std::invalid_argument("negative cardinality");
    }


    char* DeleteIdenticalChar(char *line){
        int n = (int)strlen(line);
        char *ptr1 = &line[0];
        char *ptr2 = ptr1 + 1;
        while (*ptr1 != '\0'){
            while(*ptr2 != '\0'){
                if (*ptr1 == *ptr2){
                    ptr2++;
                    n--;
                    strncpy(ptr1+1, ptr2, n-1);
                }
                else ptr2++;
            }
            ptr1++; n--;
            ptr2 = ptr1+1;
        }
        return line;
    }
    

    CharSet_dyn::CharSet_dyn(const char* line){
        if (line){
            //line = DeleteIdenticalChar(line);
            int n = (int)strlen(line);
            this->value = new char[n + 1];
            this->cardinality = n;
            strncat(this->value, line, n + 1);
        }
        else throw std::invalid_argument("empty line");
    }

    CharSet_dyn::CharSet_dyn(CharSet_dyn &charset){
        if (charset.cardinality){
            int n = charset.cardinality;
            this->cardinality = n;
            this->value = new char[n + 1];
            strncat(this->value, charset.value, n+1);
        }
        else throw std::invalid_argument("empty char set");
    }

    std::istream& operator >> (std::istream& c, CharSet_dyn& charset){
        std::string line;
        c >> line;
        charset.cardinality = 0;
        int n = line.size();
        char *new_line = new char[line.size() + 1];
        std::copy(line.begin(), line.end(), new_line);
        new_line[n] = '\0';
        new_line = DeleteIdenticalChar(new_line);
        if (!new_line) c.setstate(std::ios::failbit);
        else{
            int n = (int)strlen(new_line);
            charset.cardinality = n;
            charset.value = new char[n];
            strncat(charset.value, new_line, n + 1);
        }
        return c;
    }

std::ostream& operator << (std::ostream& s, const CharSet_dyn& charset) {
        if (charset.value){
            int n = charset.cardinality;
            std::string line = charset.value;
            s << "Set: " << line << std::endl;
            s << "Cardinality: " << n << std::endl;
        }
        else s << "The char set is empty";
        return s;
    }

bool Existence (char symbol[1], const CharSet_dyn& charset) {
        char *ptr = &charset.value[0];
        int cardinal = charset.cardinality;
        int i = 0;
        while (i < cardinal){
            if (*symbol == *ptr) return true;
            ptr++;
            i++;
        }
        return false;
    }

    CharSet_dyn& CharSet_dyn::operator= (const CharSet_dyn& charset){
        if (charset.value){
            int n = charset.cardinality;
            this->value = new char[n + 1];
            this->cardinality = n;
            strncat(this->value, charset.value, n + 1);
            return *this;
        }
        else throw std::invalid_argument("the char set doesn't exist");
    }


    CharSet_dyn& CharSet_dyn::operator= (CharSet_dyn&& charset){
        if (this != &charset){
            delete value;
            cardinality = 0;
            int n = charset.cardinality;
            value = new char[n + 1];
            strncpy(value, charset.value, n + 1);
            cardinality = n;
            charset.value = nullptr;
            charset.cardinality = 0;
            return *this;
        }
        else throw std::invalid_argument("the same object");
    }

    CharSet_dyn& operator- (CharSet_dyn& charset1, CharSet_dyn& charset2){
        if (charset2.value){
            char *ptr = &charset1.value[0];
            while (*ptr != '\0'){
                if (Existence(ptr, charset2)){
                    char *sub_ptr = ptr;
                    while (*sub_ptr != '\0'){
                        char p = *(sub_ptr + 1);
                        *sub_ptr = p;
                        sub_ptr++;
                    }
                    charset1.cardinality--;
                }
                else ptr++;
            }
        }
        return charset1;
    }

    CharSet_dyn& CharSet_dyn::operator+= (char symbol[1]){
        if (Existence(symbol, *this) == false){
            strncat(this->value, symbol, 2);
            this->cardinality++;
        }
        else throw std::invalid_argument ("This symbol exists");
        return *this;
    }

    CharSet_dyn& operator+ (CharSet_dyn& charset1, CharSet_dyn& charset2){
        charset1 - charset2;
        int n = charset2.cardinality;
        strncat(charset1.value, charset2.value, n+1);
        charset1.cardinality += n;
        return charset1;
    }

}
