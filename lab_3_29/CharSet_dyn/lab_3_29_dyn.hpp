#ifndef lab_3_29_dyn_hpp
#define lab_3_29_dyn_hpp

#include <stdio.h>
#include <iostream>


namespace ThirdLab_dyn{

    char* DeleteIdenticalChar (char *line);

    class CharSet_dyn{
    private:
        char *value;
        int cardinality;

    public:
        CharSet_dyn(int cardinal = 10);
        CharSet_dyn(const char *line);
        CharSet_dyn(CharSet_dyn &charset);
        CharSet_dyn(CharSet_dyn &&charset) : value(nullptr), cardinality(0)
        {
            if (charset.cardinality){
                int n = charset.cardinality;
                value = new char[n + 1];
                strncpy (value, charset.value, n + 1);
                cardinality = n;
                charset.value = nullptr;
                charset.cardinality = 0;
            }
            else throw std::invalid_argument("char set is empty");
        }
        friend std::istream& operator >> (std::istream& c, CharSet_dyn&);
        friend std::ostream& operator << (std::ostream& s, const CharSet_dyn& charset);
        friend bool Existence (char symbol[1], const CharSet_dyn& charset);
        CharSet_dyn& operator= (const CharSet_dyn& charset);
        CharSet_dyn& operator= (CharSet_dyn&& charset);
        friend CharSet_dyn& operator- (CharSet_dyn& charset1, CharSet_dyn& charset2);
        CharSet_dyn& operator+= (char symbol[1]);
        friend CharSet_dyn& operator* (CharSet_dyn& charset1, CharSet_dyn& charset2){
            CharSet_dyn newcharset(charset1);
            newcharset - charset2;
            charset1 - newcharset;
            return charset1;
        }
        friend CharSet_dyn& operator+ (CharSet_dyn& charset1, CharSet_dyn& charset2);
        ~CharSet_dyn(){
            delete this->value;
            this->cardinality = 0;
        }
    };
}
#endif /* lab_3_29_dyn_hpp */
