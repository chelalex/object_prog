#import <XCTest/XCTest.h>
#import "lab_3_29_dyn.hpp"
@interface Tests_dyn : XCTestCase

@end

@implementation Tests_dyn

- (void)testDefaultConstructor{
    ThirdLab_dyn::CharSet_dyn charset;
    XCTAssertNoThrow(charset);
    XCTAssertEqual(1, Existence(" ", charset));
    XCTAssertEqual(1, Existence("!", charset));
    XCTAssertEqual(1, Existence("\"", charset));
    XCTAssertEqual(1, Existence("#", charset));
    XCTAssertEqual(1, Existence("$", charset));
}

- (void)testIntConstructor{
    ThirdLab_dyn::CharSet_dyn charset(14);
    XCTAssertNoThrow(charset);
    XCTAssertEqual(1, Existence(" ", charset));
    XCTAssertEqual(1, Existence("!", charset));
    XCTAssertEqual(1, Existence("\"", charset));
    XCTAssertEqual(1, Existence("#", charset));
    XCTAssertEqual(1, Existence("$", charset));
    XCTAssertEqual(1, Existence("%", charset));
    XCTAssertEqual(1, Existence("&", charset));
    XCTAssertEqual(1, Existence("'", charset));
    XCTAssertEqual(1, Existence("(", charset));
    XCTAssertEqual(1, Existence(")", charset));
    XCTAssertEqual(1, Existence("*", charset));
    XCTAssertEqual(1, Existence("+", charset));
    XCTAssertEqual(1, Existence(",", charset));
    XCTAssertEqual(1, Existence("-", charset));
}

- (void)testCharConstructor{
    char *line = "1234567890abcd";
    ThirdLab_dyn::CharSet_dyn charset(line);
    XCTAssertNoThrow(charset);
}

- (void)testCopyConstructor{
    ThirdLab_dyn::CharSet_dyn charset(14);
    ThirdLab_dyn::CharSet_dyn charset0(charset);
    XCTAssertNoThrow(charset0);
}

- (void)testCopyAssignmentConstructor{
    ThirdLab_dyn::CharSet_dyn &&charset0 = ThirdLab_dyn::CharSet_dyn();
    XCTAssertNoThrow(charset0);
}

- (void)testElementExistence{
    ThirdLab_dyn::CharSet_dyn charset(5);
    XCTAssertEqual(1, Existence(" ", charset));
    XCTAssertEqual(1, Existence("!", charset));
    XCTAssertEqual(1, Existence("\"", charset));
    XCTAssertEqual(1, Existence("#", charset));
    XCTAssertEqual(1, Existence("$", charset));
    XCTAssertEqual(0, Existence("%", charset));
    XCTAssertEqual(0, Existence("&", charset));
    XCTAssertEqual(0, Existence("'", charset));
    XCTAssertEqual(0, Existence("(", charset));
    XCTAssertEqual(0, Existence(")", charset));
}

- (void)testAssignmentOperator{
    ThirdLab_dyn::CharSet_dyn charset;
    ThirdLab_dyn::CharSet_dyn charset0(8);
    XCTAssertNoThrow(charset0 = charset);
    XCTAssertEqual(1, Existence(" ", charset0));
    XCTAssertEqual(1, Existence("!", charset0));
    XCTAssertEqual(1, Existence("\"", charset0));
    XCTAssertEqual(1, Existence("#", charset0));
    XCTAssertEqual(1, Existence("$", charset0));
    XCTAssertEqual(1, Existence("%", charset0));
    XCTAssertEqual(1, Existence("&", charset0));
    XCTAssertEqual(1, Existence("'", charset0));
    XCTAssertEqual(1, Existence("(", charset0));
    XCTAssertEqual(1, Existence(")", charset0));
}

- (void)testSubtractionOperator{
    ThirdLab_dyn::CharSet_dyn charset;
    ThirdLab_dyn::CharSet_dyn charset0(8);
    XCTAssertNoThrow(charset - charset0);
    XCTAssertEqual(0, Existence(" ", charset));
    XCTAssertEqual(0, Existence("!", charset));
    XCTAssertEqual(0, Existence("\"", charset));
    XCTAssertEqual(0, Existence("#", charset));
    XCTAssertEqual(0, Existence("$", charset));
    XCTAssertEqual(0, Existence("%", charset));
    XCTAssertEqual(0, Existence("&", charset));
    XCTAssertEqual(0, Existence("'", charset));
    XCTAssertEqual(1, Existence("(", charset));
    XCTAssertEqual(1, Existence(")", charset));
}

- (void)testInjectionOperator{
    ThirdLab_dyn::CharSet_dyn charset0(8);
    XCTAssertNoThrow(charset0 += ",");
    XCTAssertNoThrow(charset0 += ".");
}

- (void)testMultiplicationOperator{
    char *line1 = "12345";
    char *line2 = "1asdf";
    ThirdLab_dyn::CharSet_dyn charset1(line1);
    ThirdLab_dyn::CharSet_dyn charset2(line2);
    ThirdLab_dyn::CharSet_dyn newcharset1(charset1);
    ThirdLab_dyn::CharSet_dyn newcharset2(charset1);
    newcharset1 - charset2;
    charset1 - newcharset1;
    newcharset2 * charset2;
    XCTAssertEqual(1, Existence("1", newcharset2));
    XCTAssertEqual(1, Existence("1", charset1));
    XCTAssertEqual(Existence("a",charset1), Existence("a", newcharset2));
    XCTAssertEqual(Existence("s",charset1), Existence("s", newcharset2));
    XCTAssertEqual(Existence("d",charset1), Existence("d", newcharset2));
    XCTAssertEqual(Existence("f",charset1), Existence("f", newcharset2));
}

- (void)testAddictionOperator{
    char *line1 = "12345";
    char *line2 = "1asdf";
    ThirdLab_dyn::CharSet_dyn charset1(line1);
    ThirdLab_dyn::CharSet_dyn charset2(line2);
    ThirdLab_dyn::CharSet_dyn right_charset = charset1 + charset2;
    ThirdLab_dyn::CharSet_dyn left_charset = charset1 - charset2;
    char *ptr = line2;
    for (int i = 0; i < 5; i++){
        left_charset += ptr;
        ptr++;
    }
    XCTAssertEqual(Existence("1",left_charset), Existence("1", right_charset));
    XCTAssertEqual(Existence("a",left_charset), Existence("a", right_charset));
    XCTAssertEqual(Existence("s",left_charset), Existence("s", right_charset));
}

@end
