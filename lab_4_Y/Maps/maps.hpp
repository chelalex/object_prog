#ifndef maps_hpp
#define maps_hpp

#define LINE_QUANTITY 20
#include <string>
#include <iostream>

namespace Diablo {

typedef struct square{
    std::string lines[LINE_QUANTITY];
    } square;

    typedef enum level_numbers {LEVEL_1, LEVEL_2, LEVEL_3} level_numbers;

    void map_output(square& map);
}
#endif /* maps_hpp */
