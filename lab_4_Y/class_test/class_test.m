//
//  class_test.m
//  class_test
//
//  Created by Александра Челнокова on 24.12.2020.
//

#import <XCTest/XCTest.h>
#import "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Artefact/artefact.hpp"

@interface class_test : XCTestCase

@end

@implementation class_test

- (void)testConstructor {
    Diablo::Artefact artefact;
    XCTAssertEqual(Diablo::ARTEFACT, artefact.GetName());
}

- (void)testGetModificators {
    Diablo::Artefact artefact;
    Diablo::characteristic health = {Diablo::HEALTH, 0, 0};
    std::vector <Diablo::characteristic> mod;
    mod = artefact.GetModificators();
    XCTAssertEqual(health.type, mod[0].type);
    XCTAssertEqual(health.max, mod[0].max);
    XCTAssertEqual(health.current, mod[0].current);
}

- (void)testSetModificators {
    Diablo::Artefact artefact;
    Diablo::characteristic health = {Diablo::HEALTH, 1, 1};
    XCTAssertNoThrow(artefact.SetModificator(health));
    std::vector <Diablo::characteristic> mod;
    mod = artefact.GetModificators();
    XCTAssertEqual(health.type, mod[1].type);
    XCTAssertEqual(health.max, mod[1].max);
    XCTAssertEqual(health.current, mod[1].current);
}

- (void)testClearModificators {
    Diablo::Artefact artefact;
    XCTAssertNoThrow(artefact.ClearModificators());
}

- (void)testDeleteModificator{
    Diablo::Artefact artefact;
    XCTAssertNoThrow(artefact.DeleteModificator(0));
    Diablo::characteristic health = {Diablo::HEALTH, 1, 1};
    artefact.SetModificator(health);
    XCTAssertThrows(artefact.DeleteModificator(1));
}

@end
