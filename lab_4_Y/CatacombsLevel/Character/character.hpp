#ifndef character_hpp
#define character_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Keychain/keychain.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Potion/potion.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Weapon/Artefact Weapon/artefact_weapon.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Weapon/Enchanted Weapon/enchanted_weapon.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Armor/Artefact Armor/artefact_armor.hpp"
#include <vector>
#include <iterator>

namespace Diablo{

    class Character{
    private:
        unsigned long xp;
        std::vector <characteristic> feature_table;
        std::vector <Item> bag;
    public:
        Character();
        unsigned long GetExperience();
        void SetExperience(unsigned long xp);
        std::vector <characteristic> GetFeatures();
        void SetFeature(characteristic& feat);
        virtual void GenerateDamage(Character& character);
        void SetDamage(unsigned int damage);
        std::vector <Item> GetItems();
        void DeleteItem(Item& item);
        void SetItem(Item& item);
        void ClearFeatures();
        void ClearBag();
        ~Character();
        Character& operator= (Character& character);
    };
}

#endif /* character_hpp */
