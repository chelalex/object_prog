#ifndef case_hpp
#define case_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Chest/chest.hpp"

namespace Diablo{
    typedef enum {FLOOR, OPENED_DOOR, CLOSED_DOOR, WALL, STAIRS_UP, STAIRS_DOWN, PLAYER, ENEMY} TYPE;
    
    typedef struct couple{
        int x;
        int y;
    }couple;

    static couple start = {0,0};

    class Case{
    private:
        TYPE type;
        Item* item;
        Chest* chest;
        couple coordinates;
        void DeleteChest();
        void DeleteItem();
    public:
        Case(TYPE type = FLOOR, couple& point = start);
        Chest* GetChest();
        bool SetChest(Chest* chest);
        Item* GetItem();
        bool SetItem(Item* item);
        couple GetCoordinates();
        void SetCoordinates(couple coordinates);
        TYPE GetType();
        void SetType(TYPE type);

    };
}

#endif /* case_hpp */
