//
//  enemy.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 18.11.2020.
//

#include "enemy.hpp"

namespace Diablo{

    Enemy::Enemy(enemies name){
        srand(time(0));
        this->name = name;
        this->damage_value = rand()%11 + 10;;
        this->SetExperience(rand()%101 + 120);
        unsigned int max = 1;
        characteristic health = {HEALTH, max, max};
        this->ClearFeatures();
        this->SetFeature(health);
    }

    enemies Enemy::GetName(){
        return this->name;
    }

    void Enemy::SetName (enemies name){
        this->name = name;
    }

    unsigned int Enemy::GetDamageValue(){
        return this->damage_value;
    }

    void Enemy::SetDamageValue(unsigned int value){
        this->damage_value = value;
    }

    void Enemy::GenerateDamage(Character& character){
        //character.SetDamage(this->damage_value);
        std::vector <characteristic> features = character.GetFeatures();
        std::vector <characteristic> ::iterator it = features.begin();
        while (it->type != HEALTH){
            it++;
        }
        //it->max -= this->damage_value;
        it->current -= this->damage_value;
        it = features.begin();
        character.ClearFeatures();
        while(it != features.end()){
            character.SetFeature(*it);
            it++;
        }
        
    }

    Enemy::~Enemy(){
        this->SetExperience(0);
        this->ClearFeatures();
        this->ClearBag();
        this->damage_value = 0;
    }

Enemy& Enemy::operator= (Enemy& enemy){
    this->SetExperience(enemy.GetExperience());
    std::vector <characteristic> features = enemy.GetFeatures();
    std::vector <characteristic> ::iterator feat = features.begin();
    while (feat != features.end()){
        this->SetFeature(*feat);
    }
    std::vector <Item> bag = enemy.GetItems();
    std::vector <Item> ::iterator it = bag.begin();
    while (it != bag.end()){
        this->SetItem(*it);
    }
    this->name = enemy.name;
    this->damage_value = enemy.damage_value;
    return *this;
}
}
