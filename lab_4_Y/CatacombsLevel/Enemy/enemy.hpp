#ifndef enemy_hpp
#define enemy_hpp

#include"/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Character/character.hpp"
#include <time.h>

namespace Diablo{

    class Enemy : virtual public Character{
    private:
        enemies name;
        unsigned int damage_value;
    public:
        Enemy(enemies name = HUMANLIKE);
        enemies GetName();
        void SetName (enemies name);
        unsigned int GetDamageValue();
        void SetDamageValue(unsigned int value);
        void GenerateDamage(Character& character) override;
        Enemy& operator= (Enemy& enemy);
        ~Enemy();
    };
}
#endif /* enemy_hpp */
