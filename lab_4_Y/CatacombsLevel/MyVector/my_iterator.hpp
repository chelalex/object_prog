//
//  my_iterator.hpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 22.12.2020.
//

#ifndef my_iterator_hpp
#define my_iterator_hpp

#include <iostream>

namespace Diablo {

template <class T>
class MyIterator {
        private:
            T *ptr;
        
        public:
            MyIterator() { ptr = nullptr; }
            MyIterator(T *p) { ptr = p; }
            
            T *operator + (int n)  { return (ptr + n); }
            T *operator - (int n) { return (ptr - n); }
            
            T *operator ++ (int) { return ptr++; }
            T *operator -- (int) { return ptr--; }
            T *operator ++ () { return ++ptr; }
            T *operator -- () { return --ptr; }
            
            MyIterator& operator = (const MyIterator& it) {this->ptr = it.ptr; return *this;}
            bool operator != (const MyIterator &it) noexcept { return ptr != it.ptr; }
            bool operator == (const MyIterator &it) noexcept { return ptr == it.ptr; }
            T *operator * () noexcept { return ptr; }
    }; 
}

#endif /* my_iterator_hpp */
