//
//  my_vector.hpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 10.12.2020.
//

#ifndef my_vector_hpp
#define my_vector_hpp

#include <iostream>
//#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/MyVector/my_iterator.hpp"
namespace Diablo { /**Diablo - ространство имен, которое используется во всей игре*/
    template <class T>
class MyIterator{ /*!
                   \brief MyIterator - аналог итератора из пространства std
                   \details Итератор используется для доступа к вектору MyVector, чье описание будет ниже*/
        private:
            T *ptr; /***ptr - поле, являющееся указателем на объект класса Т (Т в данном случае - шаблон)*/
        
        public:
            MyIterator() { /**MyIterator() - пустой конструктор*/
                ptr = nullptr;
                
            }
            MyIterator(T *p) {/*!\brief MyIterator(T *p) - конструктор, зависящий от указателя p на объект класса Т*
                               \details Конструктор создает итератор, присваивая полю ptr указатель p. Аргумент коструктора: указатель р на объект Т*/
                ptr = p;
            }
            
            T *operator + (int n){ /*!\brief T* operator + - перегруженный  оператор сложения
                                    \details Суть оператора заключается в том, что он перемещает указатель ptr на n вперед. Аргумент оператора: целочисленное значение n*/
                return (ptr + n);
                
            }
            T *operator - (int n){/*!\brief T* operator - - перегруженный  оператор вычитания
                                   \details Суть оператора заключается в том, что он перемещает указатель ptr на n назад. Аргумент оператора: целочисленное значение n*/
                return (ptr - n);
            }
            
            T *operator ++ (int) {/*!\brief T* operator ++ - перегруженный постинкремент
                                   \details Суть оператора заключается в том, что он перемещает указатель на один объект вперед. Аналогичен ptr + 1. Аргумент оператора: целочисленное значение*/
                return ptr++;
            }
            T *operator -- (int) {/*!\brief T* operator -- - перегруженный постдекремент
                                   \details Суть оператора заключается в том, что он перемещает указатель на один объект назад. Аналогичен ptr - 1.Аргумент оператора: целочисленное значение*/
                return ptr--;
            }
            T *operator ++ () {/*!\brief T* operator ++ - перегруженный преинкремент
                                \details Суть оператора заключается в том, что он перемещает указатель на один объект вперед. Аналогичен ptr + 1. Оператор не имеет аргументов*/
                return ++ptr;
            }
            T *operator -- () {/*!\brief T* operator -- - перегруженный предекремент
                                \details Суть оператора заключается в том, что он перемещает указатель на один объект назад. Аналогичен ptr - 1. Оператор не имеет аргументов*/
                return --ptr;
            }
            
            MyIterator& operator = (const MyIterator& it) {/*!\brief MyIterator& operator = - перегруженный оператор присваивания
                                                            \details Суть оператора заключается в том, что данному итератору, на который указывает this, присваивается поле ptr итератора it, который мы передаем в качестве аргумента*/
                this->ptr = it.ptr; return *this;
            }
            bool operator != (const MyIterator &it) noexcept {/*!\brief bool operator != - оператор неравенства
                                                               \details Суть оператора заключается в том, что сравниваются указатели итераторов this и it и возвращается истина, если они не равны, или ложь, если равны.*/
                return ptr != it.ptr;
            }
            bool operator == (const MyIterator &it) noexcept {/*!\brief bool operator == - оператор равенства
                                                               \details Суть оператора заключается в том, что сравниваются указатели итераторов this и it и возвращается ложь, если они не равны, или истина, если равны.*/
                return ptr == it.ptr;
            }
            T *operator * () noexcept {/*!\brief T* operator* - оператор, возвращающий сам указатель на объект класса T*/
                return ptr;
            }
    };

template <class T>
class MyVector{/*!\brief MyVector - шаблон, который является аналогом vector в пространстве std
                \details Отличие MyVector от std::vector заключается в том, что он используется только в тех случаях, когда требуется обработка массива не более, чем из 4 объектов класса Т. В качестве полей содержит динамический массив объектов класса Т array, константное значение size = 4, означающий максимальное возможное количество объектов, и поле currentsize, означающее занятое количество ячеек*/
    private:
        T *array;
        const unsigned int size = 4;
        unsigned int currentsize;
    public:
        MyVector() : array(nullptr), currentsize(0) {}/*!\brief MyVector() - пустой конструктор
                                                       \details Полю array присваивается значение nullptr, а полю currentsize присваивается значение 0*/
        MyVector(const unsigned int new_size){/*!\brief MyVector(const unsigned int new_size) - конструктор, создающий пустой вектор с массивом объектов класса Т  размера new_size*/
            if (new_size <= size){
                array(new T[new_size]);
                currentsize(0);
            }
            else throw std::exception();
        }
        MyVector(const T *new_array){/*!\brief MyVector(const T *new_array) - конструктрор, создающий вектор с непустым массивом объектов класса Т*/
            unsigned int arr_size = _msize(new_array)/sizeof(T);
            if (arr_size <= size){
                array(new T[arr_size]);
                for (int i = 0; i < arr_size; i++)
                    array[i] = new_array[i];
                currentsize(arr_size);
            }
            else throw std::exception();
        }
        ~MyVector(){/*! ~MyVector() - деструктор*/
            delete[] array;
            currentsize = 0;
        }
        T& operator[] (const unsigned int index){/*!\brief T& operator[] - оператор, возвращающий объект класса Т по заданному индексу const unsigned int index*/
            if (index > currentsize)
                throw std::exception();
            else
                return array[index];
        }
        //MyVector <T>& operator = (MyVector <T> &right);
        void Push_back (T &element){/*!\brief void Push_back - метод, добавляющий новый объект класса Т в конец динамического массива*/
            T *p = new T[currentsize + 1];
            for (int i = 0; i < currentsize; i++)
                p[i] = array[i];
            p[currentsize + 1] = element;
            delete[] array;
            array = p;
            currentsize++;
        }
        void Pop_back(){/*!\brief void Pop_back - метод, удаляющий в конце динамического массива объект класса Т*/
            if (currentsize){
                T *p = new T[currentsize - 1];
                for (int i = 0; i < currentsize - 1; i++)
                    p[i] = array[i];
                delete[] array;
                array = p;
                currentsize--;
            }
            else throw std::exception();
        }

        void Erase(const unsigned int index){/*!\brief void Erase(const unsigned int index) - метод, стирающий из вектора объект класса Т по заданному индексу*/
        if (index <= currentsize){
            T *p1 = new T[currentsize - 1];
            for (int i = 0; i < index; i++)
                p1[i] = array[i];
            for (int i = index + 1; i < currentsize; i++)
                p1[i] = array[i];
            delete [] array;
            array = p1;
            currentsize--;
        }
        else throw std::exception();
        }
        void Erase(const T& element){/*!\brief void Erase(const T& element) - метод, стирающий из вектора объект класса Т, который равен переданному аргументу*/
            unsigned int i = 0;
            while ((array[i] != element) && i != currentsize)
                i++;
            if ((array[i] != element) && i == currentsize)
                throw std::exception();
            else
                this->Erase(i);
            }
    
        void Clear(){/*!\brief void Clear() - метод, стирающий полностью вектор*/
        if (array)
            delete this->array;
        currentsize = 0;
        }
        unsigned int const_size(){/*!\brief unsigned int const_size() - метод, возвращающий значение максимального возможного количества элементов в векторе
                                   \return size*/
            return size;
            
        }
    
        unsigned int current_size(){/*!\brief unsigned int current_size() - метод, возвращающий текущее количество занятых ячеек*/
            return currentsize;
            
        }
        MyIterator <T> Begin(){/*!\brief MyIterator <T> Begin() - метод, возвращающий итератор, указывающий на начало вектора*/
            MyIterator <T> it(&(this->array[0]));
            return it;
        }
        MyIterator <T> End(){/*!\brief MyIterator <T> End() - метод, возвращающий итератор, указывающий на конец вектора*/
            MyIterator<T> it(&(this->array[this->currentsize]));
            return it;
        }
};
}

#endif /* my_vector_hpp */
