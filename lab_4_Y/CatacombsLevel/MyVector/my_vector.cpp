//
//  my_vector.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 10.12.2020.
//

#include "my_vector.hpp"

namespace Diablo{

    template <class T>
    MyVector<T>::MyVector(const unsigned int new_size){
        if (new_size <= size){
            array(new T[new_size]);
            currentsize(0);
        }
        else throw std::exception();
    }

    template <class T>
    MyVector<T>::MyVector(const T *new_array){
        unsigned int arr_size = _msize(new_array)/sizeof(T);
        if (arr_size <= size){
            array(new T[arr_size]);
            for (int i = 0; i < arr_size; i++)
                array[i] = new_array[i];
            currentsize(arr_size);
        }
        else throw std::exception();
    }

    template <class T>
    T& MyVector<T>::operator[](const unsigned int index){
        if (index > currentsize)
            throw std::exception();
        else
            return array[index];
    }


    /*template <class T>
    MyVector <T>& MyVector<T>::operator = (MyVector <T> &right){
        this->Clear();
        this->array = new T[right.currentsize];
        for (int i = 0; i < right.currentsize; i++)
            this->array[i] = right.array[i];
        this->currentsize = right.currentsize;
        return *this;
    }*/

    template <class T>
    void MyVector<T>::Pop_back(){
        if (currentsize){
            T *p = new T[currentsize - 1];
            for (int i = 0; i < currentsize - 1; i++)
                p[i] = array[i];
            delete[] array;
            array = p;
            currentsize--;
        }
        else throw std::exception();
    }


}
