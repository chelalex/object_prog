#ifndef catacombs_level_hpp
#define catacombs_level_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/player.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Enemy/enemy.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Case/case.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/Maps/maps.hpp"
#include <time.h>
#include <cmath>

namespace Diablo{
    
    class CatacombsLevel{
    private:
        unsigned int level_number;
        std::vector <std::vector <Case>> case_array;
        couple size;
        Player *player;
        std::vector <Enemy*> Enemies;
    public:
        CatacombsLevel(unsigned int level = 0);
        unsigned int GetLevel();
        TYPE GetPointType(couple& point);
        void SetPointType(couple& point, TYPE type, square& map, Enemy *enemy = nullptr);
        couple GetSize();
        Case& GetCase(couple &point);
        void SetCase(Case& point, couple& there);
        void SetLevel(square& map);
        Player* GetPlayer();
        void SetPlayer(Player* player);
        std::vector <Enemy*> GetEnemies();
        void SetEnemy(Enemy& enemy);
        void OpenDoor(couple& point);
        void CloseDoor(couple& point);
        /*void EraseEnemy(std::vector <Enemy> ::iterator enemy){
            this->Enemies.erase(enemy);
        }*/
    };
}

#endif /* catacombs_level_hpp */
