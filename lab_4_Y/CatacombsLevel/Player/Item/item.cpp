
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/item.hpp"
namespace Diablo{

Item::Item(){
    this->name = ITEM;
}

void Item::SetName(item_names name){
    this->name = name;
}

item_names Item::GetName() const{
    return this->name;
}

Item::~Item(){
    this->name = EMPTY;
}
}
