#ifndef potion_hpp
#define potion_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/item.hpp"
#include <time.h>

namespace Diablo{
    class Potion : public Item{
    private:
        characteristic modificator;
    public:
        Potion();
        characteristic GetModificator();
        void SetModificator(characteristic modificator);
        bool operator == (Potion& left);
        bool operator != (Potion& left);
        ~Potion();
    };
}
#endif /* potion_hpp */
