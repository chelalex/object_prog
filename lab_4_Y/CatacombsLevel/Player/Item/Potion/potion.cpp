//
//  potion.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 25.12.2020.
//

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Potion/potion.hpp"

namespace Diablo{

Potion::Potion(){
    srand(time(0));
    this->SetName(POTION);
    this->modificator.type = (feature)(rand()%4);
    this->modificator.current = rand()%21 + 10;
    this->modificator.max = this->modificator.current;
}

characteristic Potion::GetModificator(){
    return this->modificator;
}
void Potion::SetModificator(characteristic modificator){
    this->modificator = modificator;
}
Potion::~Potion(){
    this->SetName(EMPTY);
    this->modificator.current = 0;
    this->modificator.max = 0;
}

bool Potion::operator == (Potion& left){
    if (this->GetName() == left.GetName() && this->modificator.type == left.modificator.type && this->modificator.max == left.modificator.max && this->modificator.current == left.modificator.current)
        return true;
    else return false;
}

bool Potion::operator != (Potion& left){
    return !(*this == left);
}
}
