#ifndef armor_hpp
#define armor_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/item.hpp"
#include <time.h>

namespace Diablo{
    class Armor : virtual public Item{
        
        private:
            armor_type type;
            unsigned int defence_value;
        public:
            Armor();
            armor_type GetType();
            virtual unsigned int GetDefenceValue();
            virtual void SetType(armor_type type);
            virtual void SetDefenceValue(unsigned int value);
            ~Armor();
        bool operator ==(const Armor& left);
        bool operator != (const Armor& left);
    };
}
#endif /* armor_hpp */
