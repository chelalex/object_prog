//
//  artefact_armor.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 25.12.2020.
//

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Armor/Artefact Armor/artefact_armor.hpp"

namespace Diablo{

ArtefactArmor::ArtefactArmor(){
    srand(time(0));
    this->SetName(ARTEFACT_ARMOR);
    armor_type type = (armor_type)(rand()%4);
    this->SetType(type);
    int max = rand()%4;
    for (int i = 0; i <= max; i++){
        feature type = (feature)i;
        unsigned int cur = rand()%51+50;
        characteristic modificator = {type, cur, cur};
        this->SetModificator(modificator);
    }
    this->SetDefenceValue(rand()%11 + 10);
}
ArtefactArmor::~ArtefactArmor(){
    this->SetName(EMPTY);
    this->SetDefenceValue(0);
    this->ClearModificators();
}
}
