#ifndef artefact_armor_hpp
#define artefact_armor_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Armor/armor.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Artefact/artefact.hpp"
#include <vector>
namespace Diablo{
    class ArtefactArmor : public Armor, public Artefact{
        public:
        ArtefactArmor();
        ~ArtefactArmor();
    };
}

#endif /* artefact_armor_hpp */
