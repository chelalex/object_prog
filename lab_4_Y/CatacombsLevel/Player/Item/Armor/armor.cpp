//
//  armor.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 18.11.2020.
//

#include "armor.hpp"

namespace Diablo{

    Armor::Armor(){
        srand(time(0));
        this->SetName(ARMOR);
        this->type = (armor_type)(rand()%4);
        this->defence_value = rand()%11 + 10;
    }

    armor_type Armor::GetType(){
        return this->type;
    }

    unsigned int Armor::GetDefenceValue(){
        return this->defence_value;
    }

    void Armor::SetType(armor_type type){
        this->type = type;
    }

    void Armor::SetDefenceValue(unsigned int value){
        this->defence_value = value;
    }

    Armor::~Armor(){
        this->SetName(EMPTY);
        this->defence_value = 0;
    }

bool Armor::operator ==(const Armor& left){
    item_names name = left.GetName();
    armor_type type = left.type;
    unsigned int value = left.defence_value;
    if (this->GetName() == name && this->type == type && this->defence_value == value)
        return true;
    else
        return false;
}
bool Armor::operator != (const Armor& left){
    return !(*this == left);
}
}
