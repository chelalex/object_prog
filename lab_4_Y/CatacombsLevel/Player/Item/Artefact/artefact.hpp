#ifndef artefact_hpp
#define artefact_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/item.hpp"
#include <vector>
#include <iterator>
namespace Diablo{

    class Artefact: virtual public Item{
        private:
            std::vector <characteristic> modificators;
        public:
        Artefact();
        
        std::vector <characteristic> GetModificators();
        
        void SetModificator(characteristic modificator);
            
        void ClearModificators();
            
        void DeleteModificator(unsigned int pos);
        ~Artefact();
    };
}
#endif /* artefact_hpp */
