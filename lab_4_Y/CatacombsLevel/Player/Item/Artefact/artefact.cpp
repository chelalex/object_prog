//
//  artefact.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 25.12.2020.
//

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Artefact/artefact.hpp"

namespace Diablo{
Artefact::Artefact(){
    this->SetName(ARTEFACT);
}

std::vector <characteristic> Artefact::GetModificators(){
    return this->modificators;
}

void Artefact::SetModificator(characteristic modificator){
    this->modificators.push_back(modificator);
}

void Artefact::ClearModificators(){
    this->modificators.clear();
}

void Artefact::DeleteModificator(unsigned int pos){
    unsigned int size = this->modificators.size();
    if (pos > size - 1)
        throw std::invalid_argument("out of array");
    else{
        std::vector <characteristic> :: iterator i = this->modificators.begin();
        i += pos;
        this->modificators.erase(i);
    }
}
Artefact::~Artefact(){
    this->SetName(EMPTY);
    this->modificators.clear();
}
}
