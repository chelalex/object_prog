#ifndef keychain_hpp
#define keychain_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/item.hpp"
#include <time.h>

namespace Diablo{
    class Keychain : public Item{
    private:
        unsigned int key_number;
    public:
        Keychain();
        
        const unsigned int GetKeyNumber();
        
        void SetKeyNumber(unsigned int number);
        
        ~Keychain();
    };
}
#endif /* keychain_hpp */
