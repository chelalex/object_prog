//
//  keychain.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 25.12.2020.
//

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Keychain/keychain.hpp"

namespace Diablo{
Keychain::Keychain(){
    srand(time(0));
    this->SetName(KEYCHAIN);
    this->key_number = rand()%3 + 1;
}

const unsigned int Keychain::GetKeyNumber(){
    return this->key_number;
}

void Keychain::SetKeyNumber(unsigned int number){
    this->key_number = number;
}

Keychain::~Keychain(){
    this->SetName(EMPTY);
    this->key_number = 0;
}
}
