#ifndef item_hpp
#define item_hpp

#include <iostream>
#include <string>

namespace Diablo{
    
    typedef enum {EMPTY, ITEM, KEYCHAIN, ARMOR, ARTEFACT, ARTEFACT_ARMOR, WEAPON, ARTEFACT_WEAPON, ENCHANTED_WEAPON, POTION} item_names;

    const std::string str_item_names[10] = {"EMPTY", "ITEM", "KEYCHAIN", "ARMOR", "ARTEFACT", "ARTEFACT_ARMOR", "WEAPON", "ARTEFACT_WEAPON", "ENCHANTED_WEAPON", "POTION"};
    typedef enum {STRENGTH, AGILITY, STAMINA, HEALTH} feature;

    const std::string str_feature_names[4] = {"STRENGTH", "AGILITY", "STAMINA", "HEALTH"};
    
    typedef struct characteristic{
        feature type;
        unsigned int current;
        unsigned int max;
        /*struct characteristic operator=(struct characteristic& input){
            this->type = input.type;
            this->current = input.current;
            this->max = input.max;
            return *this;
        }*/
    } characteristic;

    typedef enum {HUMANLIKE, INSECT, BEAST, UNDEAD, DEVIL, ICY} enemies;

    const std::string str_enemy_names[6] = {"HUMANLIKE", "INSECT", "BEAST", "UNDEAD", "DEVIL", "ICY"};

    typedef enum {HELMET, CHESTPLATE, LEGGINGS, BOOTS} armor_type;

    const std::string str_armor_types[4] = {"HELMET", "CHESTPLATE", "LEGGINGS", "BOOTS"};

    class Item{
        private:
            item_names name;
    public:
        Item();
        
        virtual void SetName(item_names name);
        
        virtual item_names GetName() const;
        
        ~Item();
    };
}
#endif /* item_hpp */
