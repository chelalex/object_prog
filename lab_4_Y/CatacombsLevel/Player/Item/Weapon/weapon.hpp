#ifndef weapon_hpp
#define weapon_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/item.hpp"
#include <time.h>

namespace Diablo{
    class Weapon : virtual public Item{
        private:
            unsigned int damage_value;
        public:
        Weapon();
            
        virtual unsigned int GetDamageValue();
            
        virtual void SetDamageValue(unsigned int value);
        
        ~Weapon();
    };
}
#endif /* weapon_hpp */
