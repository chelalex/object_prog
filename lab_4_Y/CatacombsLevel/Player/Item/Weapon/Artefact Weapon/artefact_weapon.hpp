#ifndef artefact_weapon_hpp
#define artefact_weapon_hpp

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Weapon/weapon.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Artefact/artefact.hpp"
#include <vector>

namespace Diablo{
    class ArtefactWeapon : public Weapon, public Artefact{
        public:
        ArtefactWeapon();
        ~ArtefactWeapon();
    };
}
#endif /* artefact_weapon_hpp */
