//
//  artefact_weapon.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 25.12.2020.
//

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Weapon/Artefact Weapon/artefact_weapon.hpp"

namespace Diablo{
ArtefactWeapon::ArtefactWeapon(){
    this->SetName(ARTEFACT_WEAPON);
    int max = rand()%4;
    for (int i = 0; i <= max; i++){
        feature type = (feature)i;
        unsigned int cur = rand()%51+50;
        characteristic modificator = {type, cur, cur};
        this->SetModificator(modificator);
    }
    this->SetDamageValue(rand()%11 + 10);
    
}
ArtefactWeapon::~ArtefactWeapon(){
    this->SetName(EMPTY);
    this->SetDamageValue(0);
    this->ClearModificators();
}
}
