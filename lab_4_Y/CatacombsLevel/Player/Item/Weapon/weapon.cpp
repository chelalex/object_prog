//
//  weapon.cpp
//  lab_4_Y
//
//  Created by Александра Челнокова on 25.12.2020.
//

#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Player/Item/Weapon/weapon.hpp"

namespace Diablo{
Weapon::Weapon(){
    this->SetName(WEAPON);
    this->damage_value = rand()%11 + 10;
}

unsigned int Weapon::GetDamageValue(){
    return this->damage_value;
}

void Weapon::SetDamageValue(unsigned int value){
    this->damage_value = value;
}

Weapon::~Weapon(){
    this->SetName(EMPTY);
    this->damage_value = 0;
}
}
