#ifndef player_hpp
#define player_hpp

#include"/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Character/character.hpp"
#include"/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/Enemy/enemy.hpp"
#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/MyVector/my_vector.hpp"
//#include "/Users/aleksandracelnokova/Desktop/kaf12/lab_4_Y/CatacombsLevel/MyVector/my_iterator.hpp"
namespace Diablo{

    class Player : public Character{
    private:
        std::vector <Weapon> weapon;
        MyVector <Armor> armor;
        std::vector <Potion> potions;
        const unsigned int potion_number = 5;
        //unsigned int max_item_number;
        unsigned int key_nember;
        unsigned int DamageBonus(Enemy& enemy);
        unsigned int DefenceBonus();
    public:
        Player();
        void DrinkPotion(Potion &potion);
        void SetPotion(Potion &potion);
        unsigned int GetKeyNumber();
        void SetKeyNumber(unsigned int number);
        void TakeKeys (Keychain& keys);
        void GenerateDamage(Character& character) override;
        void UpgradeFeature(feature& name);
        std::vector <Potion> PotionList();
        std::vector <Weapon> WeaponList();
        //std::vector <Armor> ArmorList();
        //void SetWeapon(Weapon& weapon);
        //Weapon* GetWeapon();
        void TakeWeapon(Weapon& weapon);
        //void PutWeaponInBag();
        MyVector <Armor> ArmorList();
        bool TakeArmor(Armor& armor);
        //void PutArmorInBag(Armor& armor);
        ~Player();
    };
}
#endif /* player_hpp */
