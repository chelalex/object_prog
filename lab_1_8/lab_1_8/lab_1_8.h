namespace FirstLab
{
    struct Item {
        int number;
        struct Item* next;
    };
    struct Line {
        struct Item *begin;//начало списка-строки
        struct Line *next;
    };
    int Initialization(int &m, struct Line *lines);
    struct Item *MakeFibonacci (int &a);
    int getInt (int &a);
    int CreateMatrix (int &m, struct Line *lines);
    void PrintMatrix (int &m, struct Line *lines);
    void ExitLab (struct Line *lines);

}

