#include <iostream>
#include "lab_1_8.h"

using namespace FirstLab;

int main()
{
    int m;
    struct Line lines = {nullptr, nullptr};
    int check;
    check = Initialization(m, &lines);
    if (check == 1)
    {
        check = CreateMatrix(m, &lines);
        PrintMatrix(m, &lines);
    }
    else
    {
        std::cout << "Something went wrong :(" << std::endl;
        return 1;
    }
    ExitLab (&lines);
    return 0;
}
