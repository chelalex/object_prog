#include <iostream>
#include "lab_1_8.h"
namespace FirstLab
{
    int getInt (int &a)//flag - вспомагательная переменная, чтобы различать, что вводится: кол-во строк (>0) или сами числа (>=0)
    {
        std::cin >> a;
        if ((a <= 0) || (!std::cin.good()))
            return -1;
        return 1;
    }
    
    int Initialization(int &m, struct Line *lines)
    {
        int check;
        char* msg = "";
        std::cout << "Print the number of lines: ";
        do{
            std::cout << msg << std::endl;
            check = getInt(m);
            msg = "Error! Please, try again!";
        }while ((check == -1)||(!m));
        struct Line *current_line = lines;
        for (int i = 0; i < m; i++){
            current_line->begin = nullptr;
            current_line->next = new struct Line;
            current_line = current_line->next;
        }
        if (lines) return 1;
        else return -1;
    }

    struct Item *MakeFibonacci (int &a) //разложение числа в строку чисел Фибоначчи
    {
        struct Item head = {-1, nullptr};
        struct Item* previous = &head;
        struct Item* current = nullptr;
        while (a != 0)
        {
            int F[2];
            F[0] = 0;
            F[1] = 1;
            int i = 0;
            while (F[0]+F[1] <= a)
            {
                F[i%2] = F[0]+F[1];
                i++;
            }
            current = new struct Item;
            current->number = F[(i+1)%2];
            current->next = nullptr;
            previous->next = current;
            previous = previous->next;
            current = current->next;
            a -= F[(i+1)%2];
        }
        return head.next;
    }
    
    int CreateMatrix (int &m, struct Line *lines)
    {
        int a;
        std::cout << "Enter " << m << " numbers:" << std::endl;
        int check;
        char* msg = "";
        struct Line *current = lines;
        for (int i = 0; i < m; i++)
        {
            struct Item *current_line = nullptr;
            do{
                std::cout << msg << std::endl;
                check = getInt(a);
                msg = "Error! Please, try again!";
            }while (check == -1);
            if (check == 1) msg = "";
            current_line = MakeFibonacci(a);
            current->begin = current_line;
            current = current->next;
        }
        return 1;
    }

    void PrintMatrix (int &m, struct Line *lines)
    {
        std::cout << "Your matrix is:" << std::endl;
        struct Line *current_line = lines;
        for (int i = 0; i < m; i++, current_line = current_line->next)
        {
            struct Item *current_item = current_line->begin;
            while (current_item)
            {
                std::cout << current_item->number << " ";
                current_item = current_item->next;
            }
            std::cout << std::endl;
        }
    }
    
    void ExitLab (struct Line *lines)
    {
        std::cout << "Goodbye! See you soon!" << std::endl;
        lines = nullptr;
    }
};
